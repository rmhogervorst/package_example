



# Bug reports
## due diligence
BEFORE you submit a bugreport:
* make sure you are on the latest version
* try older verions
* search the bugtracker 
perhaps someone else had the same problem?

## What to put in your bug report

* What version of the programming language are you using?

use `devtools::session_info()` or `sessionInfo()`

* what operating system are you on? 
Windows? (Vista? 7? 32-bit? 64-bit?) Mac OS X? (10.7.4? 10.9.0?) Linux? (Which distro? Which version of that distro? 32 or 64 bits?) Again, more detail is better.

* Which version of the software are you using?
Session info will give you some inkling . 

* How can de developers recreate the bug on their end?
If possible, include a copy of your code, the command you used to invoke it, and the full output of your run (if applicable.)
A common tactic is to pare down your code until a simple (but still bug-causing) “base case” remains. Not only can this help you identify problems which aren’t real bugs, but it means the developer can get to fixing the bug faster.


# Contributing changes
### version control
Always make a new branch
don't submit unrelated changes in the same branch/pull request

### Code formatting
follow the style in the main repository (this is usually google style guide/ hadley style guide)

### Documentation is not optional
* New features should ideally include updates to prose documentation, including useful example code snippets.
* All submissions should have a changelog entry crediting the contributor and/or any individuals instrumental in identifying the problem.

### add tests
Create a test that catches the bug 
fix the issue and make sure the test is now passed



# Sources
*based on http://contribution-guide-org.readthedocs.io/*
*and the excellent [ropensci work](https://github.com/ropensci/onboarding/blob/master/packaging_guide.md)*